﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace REST_API.MOODA
{
    public class ActionSelectionModule : BasicModule
    {
        public ActionSelectionModule()
        {
            ModuleName = "Action Selection";
        }
    }
}
