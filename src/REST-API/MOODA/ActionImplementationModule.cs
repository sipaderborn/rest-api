﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace REST_API.MOODA
{
    public class ActionImplementationModule : BasicModule
    {
        public ActionImplementationModule()
        {
            ModuleName = "Action Implementation";
        }

    }
}
