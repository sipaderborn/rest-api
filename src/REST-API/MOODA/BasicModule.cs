﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace REST_API.MOODA
{
    public abstract class BasicModule
    {
        /// <summary>
        /// Module Name
        /// </summary>
        public string ModuleName { get; protected set; }

        /// <summary>
        /// URI to the process to be used
        /// </summary>
        protected Process Process { get; set; }

        /// <summary>
        /// State of the process
        /// </summary>
        protected ProcessState State { get; set; }

        /// <summary>
        /// Criteria-based Control to decide if Module can be leaved
        /// </summary>
        protected ModuleControl Control { get; set; }

        // Internal Feedback-Loop has to be added
        // External Feedback-Loop has to be implemented via Delegates or something similar
    }
}
