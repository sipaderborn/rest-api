﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace REST_API.MOODA
{
    public class SituationUnderstandingModule : BasicModule
    {
        public SituationUnderstandingModule()
        {
            ModuleName = "Situation Understanding";
        }
    }
}
